# Copyright (c) 2019-2021 Oracle and/or its affiliates. All rights reserved.
# Licensed under the Universal Permissive License v 1.0 as shown at http://oss.oracle.com/licenses/upl.
#

#output "lb_public_url" {
#  value = format("http://%s", lookup(oci_load_balancer_load_balancer.prestashop_lb.ip_address_details[0], "ip_address"))
#}


output "comments" {
  value = "The application URL will be available in a few minutes. Update first your DNS to point this domain to ${local.prestashop_dns_endpoint}"
}

output "prestashop_url_https" {
  value       = format("https://%s", var.prestashop_domain)
  description = "Prestashop URL"
}

output "lb_public_ip" {
  value =  oci_load_balancer_load_balancer.prestashop_lb.ip_address_details[*].ip_address
}

output "bastion_public_ip" {
  description = "Bastion Public IP: "
  value       = oci_core_instance.prestashop_bastion.*.public_ip
}

output "waf_public_cname" {
  description = "WAF Public CNAME: "
  value       = local.prestashop_waf_cname
}

locals {
  prestashop_dns_endpoint    = var.use_waf ?  data.oci_waas_waas_policy.prestashop_waas_policy[0].cname : oci_load_balancer_load_balancer.prestashop_lb.ip_address_details[0].ip_address
  prestashop_waf_cname       = var.use_waf ?  data.oci_waas_waas_policy.prestashop_waas_policy[0].cname : "No WAF deployed"
}
