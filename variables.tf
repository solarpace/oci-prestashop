# OCI Provider
variable "tenancy_ocid" {}
variable "compartment_ocid" {}
variable "region" {}
variable "user_ocid" {
  default = ""
}
variable "fingerprint" {
  default = ""
}
variable "private_key_path" {
  default = ""
}

variable "instance_shape" {
  default = "VM.Standard.E4.Flex"
}
variable "instance_ocpus" {
  default = 1
}
variable "instance_shape_config_memory_in_gbs" {
  default = 16
}
variable "image_operating_system" {
  default = "Oracle Linux"
}
variable "image_operating_system_version" {
  default = "7.9"
}

variable "instance_public_sshkey" {
  default = ""
}

variable "bastion_source_image_id" {
  # image Oracle-Linux-7.9-aarch64-2021.05.17-0
  default = "ocid1.image.oc1.eu-frankfurt-1.aaaaaaaaajkxjdpgfzjl7tg3a7vzdvwnww6w5k47r5acwe4fqecowqwuoria"
}
variable "network_cidrs" {
  type = map(string)

  default = {
    MAIN-VCN-CIDR                = "10.1.0.0/16"
    DB-SUBNET-REGIONAL-CIDR      = "10.1.1.0/24"
    APP-SUBNET-REGIONAL-CIDR     = "10.1.2.0/24"
    LB-SUBNET-REGIONAL-CIDR      = "10.1.3.0/24"
    BASTION-SUBNET-REGIONAL-CIDR = "10.1.4.0/24"
    LB-VCN-CIDR                  = "10.2.0.0/16"
    ALL-CIDR                     = "0.0.0.0/0"
    HOME-CIDR                    = "82.65.89.141/32"
    BASTION-CIDR                 = "10.1.3.145/32"
  }
}

variable "network_ips" {
  type = map(string)

  default = {
    MYSQL-IP              = "10.1.1.144"
    LB-IP                 = "10.1.1.144"
    FS-IP                 = "10.1.2.7"
    BASTION-IP            = "10.1.3.145"
  }
}
variable "environment" {
  default = "dev"
}
variable "instance_visibility" {
  default = "Private"
}

variable "prestashop_custimg_url" {
    default = ""
}

variable "prestashop_fs_data_url" {
  default = ""
}

variable "prestashop_domain" {
    default = ""
}

variable "lb_shape" {
  default = "flexible"
}
variable "lb_shape_details_minimum_bandwidth_in_mbps" {
  default = 10
}
variable "lb_shape_details_maximum_bandwidth_in_mbps" {
  default = 100
}

variable "lb_private_key" {
  default = ""
}
variable "lb_public_certificate" {
  default = ""
}

variable "mysql_cluster" {
  default = "true"
}
variable "mysql_password" {
  default = ""
}

variable "mysql_username" {
  default = "admin"
}

variable "psdb_dbname" {
  default = "prestashoptest"
}
variable "psdb_password" {
  default = ""
}

variable "psdb_username" {
  default = "prestashopuser"
}

variable "psdb_par_url" {
  default = ""
}

variable "mysql_configuration_ocid" {
  default = ""
}

variable "mysql_shape_name" {
  default = "MySQL.VM.Standard.E3.1.8GB"
}

variable "mysql_fault_domain" {
  default = "FAULT-DOMAIN-2"
}


variable "image_image_source_details_operating_system" {
  default = "Canonical Ubuntu"
}
variable "image_image_source_details_operating_system_version" {
  default = "20.04"
}
variable "source_image_type" {
  default = "QCOW2"
}

# WAF
variable "use_waf" {
  default = false
}
variable "create_appserver" {
  default = false
}
variable "deploy_in_region" {
  default = false
}
variable "lb_https" {
  default = false
}

locals {
  instance_shape                             = var.instance_shape
  lb_shape                                   = var.lb_shape
  lb_shape_details_minimum_bandwidth_in_mbps = var.lb_shape_details_minimum_bandwidth_in_mbps
  lb_shape_details_maximum_bandwidth_in_mbps = var.lb_shape_details_maximum_bandwidth_in_mbps
  region_to_deploy                           = var.region
}

# Shapes
locals {
  compute_flexible_shapes = [
    "VM.Standard.E3.Flex",
    "VM.Standard.E4.Flex",
    "VM.Standard.A1.Flex"
  ]
  compute_shape_flexible_descriptions = [
    "Cores for Standard.E3.Flex and BM.Standard.E3.128 Instances",
    "Cores for Standard.E4.Flex and BM.Standard.E4.128 Instances",
    "Cores for Standard.A1 based VM and BM Instances"
  ]
  compute_arm_shapes = [
    "VM.Standard.A1.Flex",
    "BM.Standard.A1.160"
  ]
  compute_shape_flexible_vs_descriptions = zipmap(local.compute_flexible_shapes, local.compute_shape_flexible_descriptions)
  compute_shape_description              = lookup(local.compute_shape_flexible_vs_descriptions, local.instance_shape, local.instance_shape)
  compute_platform                       = contains(local.compute_arm_shapes, var.instance_shape) ? "linux/arm64" : "linux/amd64"
  lb_shape_flexible                      = "flexible"
  compute_arm_shape_check = local.compute_platform == "linux/arm64" ? (contains(local.compute_arm_shapes, local.instance_shape) ? 0 : (
  file("ERROR: Selected compute shape (${local.instance_shape}) not compatible with Prestashop ${local.compute_platform} stack"))) : 0
}
