# Copyright (c) 2019, 2020 Oracle and/or its affiliates. All rights reserved.
# Licensed under the Universal Permissive License v 1.0 as shown at http://oss.oracle.com/licenses/upl.
#

resource "oci_core_virtual_network" "prestashop_main_vcn" {
  cidr_block     = lookup(var.network_cidrs, "MAIN-VCN-CIDR")
  compartment_id = var.compartment_ocid
  display_name   = "vcn_prestashop_${var.environment}"
  dns_label      = "vcnps${var.environment}"
  freeform_tags  = local.common_tags
}

resource oci_core_default_dhcp_options prestashop_dhcp_options {
  compartment_id = var.compartment_ocid
  freeform_tags  = local.common_tags
  display_name     = "Default DHCP Options for Prestashop VCN"
  domain_name_type = "CUSTOM_DOMAIN"
  manage_default_resource_id = oci_core_virtual_network.prestashop_main_vcn.default_dhcp_options_id
  options {
    custom_dns_servers = [
    ]
    #search_domain_names = <<Optional value not found in discovery>>
    server_type = "VcnLocalPlusInternet"
    type        = "DomainNameServer"
  }
  options {
    #custom_dns_servers = <<Optional value not found in discovery>>
    search_domain_names = [
      "prestashop.oraclevcn.com",
    ]
    #server_type = <<Optional value not found in discovery>>
    type = "SearchDomain"
  }
}

resource oci_core_internet_gateway prestashop_internet_gateway {
  compartment_id = var.compartment_ocid
  freeform_tags  = local.common_tags
  display_name   = "igw_prestashop_${var.environment}"
  enabled        = "true"
  vcn_id = oci_core_virtual_network.prestashop_main_vcn.id
}

resource oci_core_nat_gateway prestashop_nat_gateway {
  block_traffic  = "false"
  compartment_id = var.compartment_ocid
  freeform_tags  = local.common_tags
  display_name   = "nat_prestashop_${var.environment}"
#  public_ip_id = "ocid1.publicip.oc1.eu-frankfurt-1.aaaaaaaas6xemoyobo6fzr4g2nnxh2f7kvdorpyubowki4txfzppsr366flq"
  vcn_id         = oci_core_virtual_network.prestashop_main_vcn.id
}



resource "oci_core_service_gateway" "prestashop_service_gateway" {
  compartment_id = var.compartment_ocid
  freeform_tags  = local.common_tags
  display_name   = "sgw_prestashop_${var.environment}"
  services {
    service_id = lookup(data.oci_core_services.all_services.services[0], "id")
  }
  vcn_id         = oci_core_virtual_network.prestashop_main_vcn.id
}

resource "oci_core_route_table" "prestashop_internal_route_table" {
  compartment_id = var.compartment_ocid
  vcn_id         = oci_core_virtual_network.prestashop_main_vcn.id
  display_name   = "rt_prestashop_priv_${var.environment}"
  freeform_tags  = local.common_tags

  dynamic "route_rules" {
    for_each = (var.instance_visibility == "Private") ? [1] : []
    content {
      destination       = lookup(data.oci_core_services.all_services.services[0], "cidr_block")
      destination_type  = "SERVICE_CIDR_BLOCK"
      network_entity_id = oci_core_service_gateway.prestashop_service_gateway.id
    }
  }

  dynamic "route_rules" {
    for_each = (var.instance_visibility == "Private") ? [] : [1]
    content {
      destination       = lookup(var.network_cidrs, "ALL-CIDR")
      destination_type  = "CIDR_BLOCK"
      network_entity_id = oci_core_internet_gateway.prestashop_internet_gateway.id
    }
  }
}

resource oci_core_route_table prestashop_internet_route_table {
  compartment_id = var.compartment_ocid
  freeform_tags  = local.common_tags
  display_name   = "rt_prestashop_pub_${var.environment}"
  route_rules {
    #description = <<Optional value not found in discovery>>
    destination       = lookup(var.network_cidrs, "ALL-CIDR")
    destination_type  = "CIDR_BLOCK"
    network_entity_id = oci_core_internet_gateway.prestashop_internet_gateway.id
  }
  vcn_id = oci_core_virtual_network.prestashop_main_vcn.id
}


resource oci_core_subnet prestashop_subnet_db {
  availability_domain        = var.deploy_in_region ? "" : data.oci_identity_availability_domain.region_ad3.name
  cidr_block                 = lookup(var.network_cidrs, "DB-SUBNET-REGIONAL-CIDR")
  dhcp_options_id            = oci_core_virtual_network.prestashop_main_vcn.default_dhcp_options_id
  compartment_id             = var.compartment_ocid
  display_name               = "sub_prestashop-db_${var.environment}"
  dns_label                  = "subpsdb${var.environment}"
  freeform_tags              = local.common_tags
  prohibit_internet_ingress  = "true"
  prohibit_public_ip_on_vnic = "true"
  route_table_id             = oci_core_route_table.prestashop_internal_route_table.id
  security_list_ids = [
    oci_core_security_list.prestashop_seclist_db.id,
  ]
  vcn_id = oci_core_virtual_network.prestashop_main_vcn.id
}

resource oci_core_subnet prestashop_subnet_app {
  cidr_block                 = lookup(var.network_cidrs, "APP-SUBNET-REGIONAL-CIDR")
  dhcp_options_id            = oci_core_virtual_network.prestashop_main_vcn.default_dhcp_options_id
  compartment_id             = var.compartment_ocid
  display_name               = "sub_prestashop-app_${var.environment}"
  dns_label                  = "subpsapp${var.environment}"
  freeform_tags              = local.common_tags
  prohibit_internet_ingress  = "true"
  prohibit_public_ip_on_vnic = "true"
  route_table_id             = oci_core_route_table.prestashop_internal_route_table.id
  security_list_ids = [
    oci_core_security_list.prestashop_seclist_app.id,
  ]
  vcn_id = oci_core_virtual_network.prestashop_main_vcn.id
}


resource oci_core_subnet prestashop_subnet_lb {
  cidr_block                 = lookup(var.network_cidrs, "LB-SUBNET-REGIONAL-CIDR")
  dhcp_options_id            = oci_core_virtual_network.prestashop_main_vcn.default_dhcp_options_id
  compartment_id             = var.compartment_ocid
  display_name               = "sub_prestashop-lb_${var.environment}"
  dns_label                  = "subppslb${var.environment}"
  freeform_tags              = local.common_tags
  prohibit_internet_ingress  = "false"
  prohibit_public_ip_on_vnic = "false"
  route_table_id             = oci_core_route_table.prestashop_internet_route_table.id
  security_list_ids = [
    oci_core_security_list.prestashop_seclist_lb.id
  ]
  vcn_id = oci_core_virtual_network.prestashop_main_vcn.id
}

resource oci_core_subnet prestashop_subnet_bastion {
  cidr_block                 = lookup(var.network_cidrs, "BASTION-SUBNET-REGIONAL-CIDR")
  dhcp_options_id            = oci_core_virtual_network.prestashop_main_vcn.default_dhcp_options_id
  compartment_id             = var.compartment_ocid
  display_name               = "sub_prestashop-bs_${var.environment}"
  dns_label                  = "subppsbs${var.environment}"
  freeform_tags              = local.common_tags
  prohibit_internet_ingress  = "false"
  prohibit_public_ip_on_vnic = "false"
  route_table_id             = oci_core_route_table.prestashop_internet_route_table.id
  security_list_ids = [
    oci_core_security_list.prestashop_seclist_bs.id
  ]
  vcn_id = oci_core_virtual_network.prestashop_main_vcn.id
}

resource oci_core_security_list prestashop_seclist_lb {
  compartment_id             = var.compartment_ocid
  display_name               = "sl_prestashop-lb_${var.environment}"
  freeform_tags              = local.common_tags
  egress_security_rules {
    description      = "All all egress"
    destination      = lookup(var.network_cidrs, "ALL-CIDR")
    destination_type = "CIDR_BLOCK"
    protocol  = "6"
    stateless = "false"
  }
  egress_security_rules {
    #description = <<Optional value not found in discovery>>
    destination      = lookup(var.network_cidrs, "ALL-CIDR")
    destination_type = "CIDR_BLOCK"
    protocol  = "1"
    stateless = "false"
  }
  egress_security_rules {
    #description = <<Optional value not found in discovery>>
    destination      = lookup(var.network_cidrs, "APP-SUBNET-REGIONAL-CIDR")
    destination_type = "CIDR_BLOCK"
    protocol  = "6"
    stateless = "false"
  }
  egress_security_rules {
    destination      = lookup(var.network_cidrs, "APP-SUBNET-REGIONAL-CIDR")
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "6"
    stateless = "false"
    tcp_options {
      max = "80"
      min = "80"
    }
  }
  ingress_security_rules {
    description = "Allow HTTP traffic from Internet"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = lookup(var.network_cidrs, "ALL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "80"
      min = "80"
    }
  }
  ingress_security_rules {
    protocol    = "6"
    source      = lookup(var.network_cidrs, "APP-SUBNET-REGIONAL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
  }
  ingress_security_rules {
    description = "Allow HTTPS"
    protocol    = "6"
    source      = lookup(var.network_cidrs, "ALL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "443"
      min = "443"
    }
  }
  vcn_id = oci_core_virtual_network.prestashop_main_vcn.id
}

resource oci_core_security_list prestashop_seclist_bs {
  compartment_id             = var.compartment_ocid
  display_name               = "sl_prestashop-bs_${var.environment}"
  freeform_tags              = local.common_tags
  egress_security_rules {
    description      = "All all egress"
    destination      = lookup(var.network_cidrs, "ALL-CIDR")
    destination_type = "CIDR_BLOCK"
    protocol  = "6"
    stateless = "false"
  }
  egress_security_rules {
    #description = <<Optional value not found in discovery>>
    destination      = lookup(var.network_cidrs, "ALL-CIDR")
    destination_type = "CIDR_BLOCK"
    protocol  = "1"
    stateless = "false"
  }
  egress_security_rules {
    #description = <<Optional value not found in discovery>>
    destination      = lookup(var.network_cidrs, "APP-SUBNET-REGIONAL-CIDR")
    destination_type = "CIDR_BLOCK"
    protocol  = "6"
    stateless = "false"
  }
  ingress_security_rules {
    protocol    = "6"
    source      = lookup(var.network_cidrs, "APP-SUBNET-REGIONAL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
  }
  ingress_security_rules {
    description = "Allow SSH from home"
    protocol    = "6"
    source      = lookup(var.network_cidrs, "HOME-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "22"
      min = "22"
    }
  }
  vcn_id = oci_core_virtual_network.prestashop_main_vcn.id
}

resource oci_core_security_list prestashop_seclist_db {
  compartment_id             = var.compartment_ocid
  display_name               = "sl_prestashop-db_${var.environment}"
  freeform_tags              = local.common_tags
  ingress_security_rules {
    description = "Allow SSH from App Subnet"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = lookup(var.network_cidrs, "APP-SUBNET-REGIONAL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "22"
      min = "22"
    }
  }
  ingress_security_rules {
    description = "Allow Mysql from App Subnet "
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = lookup(var.network_cidrs, "APP-SUBNET-REGIONAL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "3306"
      min = "3306"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    description = "Allow MySQL from Bastion"
    protocol    = "6"
    source      = lookup(var.network_cidrs, "BASTION-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "3306"
      min = "3306"
    }
  }
  ingress_security_rules {
    description = "Secure Mysql  access from App Subnet"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = lookup(var.network_cidrs, "APP-SUBNET-REGIONAL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "33060"
      min = "33060"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  vcn_id = oci_core_virtual_network.prestashop_main_vcn.id
}

resource oci_core_security_list prestashop_seclist_app {
  compartment_id             = var.compartment_ocid
  display_name               = "sl_prestashop-app_${var.environment}"
  freeform_tags              = local.common_tags
  egress_security_rules {
    description      = "AL"
    destination      = lookup(var.network_cidrs, "ALL-CIDR")
    destination_type = "CIDR_BLOCK"
    protocol  = "all"
    stateless = "false"
  }
  egress_security_rules {
    destination      = lookup(var.network_cidrs, "LB-SUBNET-REGIONAL-CIDR")
    destination_type = "CIDR_BLOCK"
    protocol         = "6"
    stateless        = "false"
  }
  ingress_security_rules {
    description = "Allow SSH from Bastion"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = lookup(var.network_cidrs, "BASTION-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "22"
      min = "22"
    }
  }
  ingress_security_rules {
    description = "Allow ping from Bastion"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "1"
    source      = lookup(var.network_cidrs, "BASTION-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
  }
  ingress_security_rules {
    description = "Allow HTTP from LB Subnet"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = lookup(var.network_cidrs, "LB-SUBNET-REGIONAL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "80"
      min = "80"
    }
  }
  ingress_security_rules {
    description = "Allow NFS protocols"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = lookup(var.network_cidrs, "APP-SUBNET-REGIONAL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "111"
      min = "111"
    }
  }
  ingress_security_rules {
    description = "Allow NFS protocols"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = lookup(var.network_cidrs, "APP-SUBNET-REGIONAL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "2048"
      min = "2048"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    description = "Allow NFS protocols"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = lookup(var.network_cidrs, "APP-SUBNET-REGIONAL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "2049"
      min = "2049"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    description = "Allow NFS protocols"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = lookup(var.network_cidrs, "APP-SUBNET-REGIONAL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "2050"
      min = "2050"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    description = "Allow NFS protocols UDP"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "17"
    source      = lookup(var.network_cidrs, "APP-SUBNET-REGIONAL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    udp_options {
      max = "111"
      min = "111"
      #source_port_range = <<Optional value not found in discovery>>
    }
  }
  ingress_security_rules {
    description = "Allow NFS protocols UDP"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "17"
    source      = lookup(var.network_cidrs, "APP-SUBNET-REGIONAL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    udp_options {
      max = "2048"
      min = "2048"
    }
  }






  ingress_security_rules {
    description = "Allow NFS protocols"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = lookup(var.network_cidrs, "BASTION-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "111"
      min = "111"
    }
  }
  ingress_security_rules {
    description = "Allow NFS protocols"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = lookup(var.network_cidrs, "BASTION-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "2048"
      min = "2048"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    description = "Allow NFS protocols"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = lookup(var.network_cidrs, "BASTION-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "2049"
      min = "2049"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    description = "Allow NFS protocols"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = lookup(var.network_cidrs, "BASTION-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "2050"
      min = "2050"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    description = "Allow NFS protocols UDP"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "17"
    source      = lookup(var.network_cidrs, "BASTION-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    udp_options {
      max = "111"
      min = "111"
      #source_port_range = <<Optional value not found in discovery>>
    }
  }
  ingress_security_rules {
    description = "Allow NFS protocols UDP"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "17"
    source      = lookup(var.network_cidrs, "BASTION-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    udp_options {
      max = "2048"
      min = "2048"
    }
  }

  vcn_id = oci_core_virtual_network.prestashop_main_vcn.id
}
