# ![Prestashop Logo](./images/prestashop-logo.png)

This project is a Terraform configuration that deploys a scalable, higly-available Prestashop platform on [Oracle Cloud Infrastructure][oci].


## Getting Started with oci-prestashop

The repository contains the application code as well as the [Terraform][tf] code to create all the required resources and configures the application on the created resources.

The steps below guide you through deploying the application on your tenancy using terraform on your local machine. You can also zip the files and use the OCI Resource Manager to create/deploy a stack by importing the zip file.

### Prerequisites

>> Note version 0.0.2 : this part has been removed and will be replaced soon by a fresh Prestashop install instead. Existing version only deploys OCI resources without application instances

~~For now the project has some dependencies on preconfigured Prestashop artifacts saved on an OCI Bucket and shared with Pre-authenticated (PAR) URLs; those URLs need to be used in the terraform.tfvars:~~

  - ~~Compute Custom Image : a Prestashop 1.7 install has been already done an exported as a custom image~~

    > ~~prestashop_custimg_url="https://objectstorage.eu-frankfurt-1.oraclecloud.com/p/9Fpz0kejGAg6ivD8l-DomigEZ3ukakNLPjlt34yblLnK9DCD4DUUDBzZTZen5cBh/n/oraseemeafrtech1/b/cfpr-custom-images/o/exported-custimg-prestashop-20210918"~~

  - ~~Database schema and data: the preconfigured Prestashop database has been exported and also shared as a PAR URL. The export has been done directly on an OCI bucket using [MySql Shell](mshell)~~

    > ~~util.dumpSchemas(["<your database>"],"",{"osBucketName": "<your bucket", "osNamespace": "<your namespace>", "ocimds": "true", "compatibility": ["strip_definers", "strip_restricted_grants","ignore_missing_pks"]})~~

    > ~~psdb_par_url = "https://objectstorage.eu-frankfurt-1.oraclecloud.com/p/DhIZ8S26Hs7_dzEBicQkYR8jQbipVYCmxMXHNChGkuUbb9K8itT3iLufbQOYVvgf/n/oraseemeafrtech1/b/mysql-bucket/o/@.manifest.json"~~

  - ~~Prestashop Shared files: to create an highly-available Prestashop instance we need to share files between the different compute instances. This is done using an OCI File Storage. The files related to the preconfigured Prestastop install have been exported also in a PAR and will be automatically imported and shared with NFS between the OCI compute instances.~~

    > ~~prestashop_fs_data_url = "https://objectstorage.eu-frankfurt-1.oraclecloud.com/p/Ewl2i_dv8eYKsJWT55XdfxSVajcB-AgodLKMFYgIpvLYm9oiJ5pzkd446FbSqSVx/n/oraseemeafrtech1/b/prestashop-data-bucket/o/fs-prestashop.tar.gz"~~

1. Clone the [oci-prestashop](git-project) project

```bash
git clone https://gitlab.com/solarpace/oci-prestashop.git
```

2. Configure your deployment by creating the file **terraform.tf** using the template file  **terraform.tf.sample** and updating the variables based on your target configuration:

```shell
# OCI authentication
user_ocid         = "ocid1.user.oc1..aaxxxxxxxxxxxxxxxxxxxxxxxxx"
fingerprint       = "de:d7:b4:db:d9:97:xx:xx:xxx:xx:xx:xx:xx"
tenancy_ocid      = "ocid1.tenancy.oc1..aaxxxxxxxxxxxxxxxxxxxxxxxxx"
private_key_path  = "/path/.oci/oci_api_key.pem"
# region
region = "eu-frankfurt-1"

# Deployment compartment
compartment_ocid = "ocid1.compartment.oc1..aaxxxxxxxxxxxxxxxxxxxxxxxxx"

mysql_username = "admin"
mysql_password = "Ecom1l4aaxxxxxxxx"
# Pretashop database (created by Bastion Cloud-init script - see scripts folder)
psdb_dbname = "prestashoptest"
psdb_password = "Ecomxxxxxxxxxxxxxx"

# front-end web domain
prestashop_domain="prestashopgvo.solarpace.io"

# SSH Public key for Bastion
instance_public_sshkey = "ssh-rsa AAAAB3Nzaaaxxxxxxxxxxxxxxxxxxxxxxxxx"

# Certificate for LB and WAF if lb_https = true, use_waf = true
lb_private_key = ""
lb_public_certificate = ""

# Instance Shape for AppServer (only if create_appserver = true)
instance_shape = "VM.Standard.E4.Flex"

network_cidrs = {
    MAIN-VCN-CIDR                = "10.0.0.0/16"
    DB-SUBNET-REGIONAL-CIDR      = "10.0.1.0/24"
    APP-SUBNET-REGIONAL-CIDR     = "10.0.2.0/24"
    LB-SUBNET-REGIONAL-CIDR      = "10.0.3.0/24"
    BASTION-SUBNET-REGIONAL-CIDR = "10.0.4.0/24"
    ALL-CIDR                     = "0.0.0.0/0"
    HOME-CIDR                    = "0.0.0.0/0"
    BASTION-CIDR                 = "10.0.4.145/32"
  }

network_ips = {
      MYSQL-IP              = "10.0.1.144"
      FS-IP                 = "10.0.2.7"
      BASTION-IP            = "10.0.4.145"
}

## Control deployment options
# For Testing disable MySQL Cluster and WAF:
mysql_cluster = "true"
# Deploy WAF
use_waf = "false"
# Deploy Appserver from custom image
create_appserver = "false"
# Choose deployment in single AD (false) or region (true)
deploy_in_region = "false"
# Use HTTPS
lb_https = "false"
```

5. Deploy the Terraform stack with the following commands:

```shell
terraform plan
terraform apply -auto-approve
```


> The application is being deployed to the compute instances asynchronously, and it may take a couple of minutes for the URL to serve the application. Meanwhile, if you defined a non-existing public domain, update your DNS configuration or host file to point to the frontend IP:

```
<LB or WAF Frontend IP>     <domain name>
```

### cloud-init script

This project takes an existing Prestashop configuration (database, shared files, custom image) and deploy a new application on a new domain URL.

To achieve this it is required to run some scripts to populate our target application; those scripts will take care of the following tasks:

  * create the new MySql database and user (psdb_dbname, psdb_username)
~~  * import the database schema and data~~
~~  * Update the domain configuration in Prestashop tables (ps_configuration, ps_shop_url)~~
~~  * import the shared files in the NFS File Storage~~
~~  * Update the configuration file parameters.php with the MySql connection values (psdb_dbname, psdb_username, psdb_password)~~

A Bastion server is created mainly for this with a cloud-init script defined in _user_data_ (see [bastion.tf](./bastion.tf) file). Using the files in the _scripts_ folder, Terraform will :

  * update the variables in the script files as defined in the [datasouraces.tf](./datasources.tf) file
  * transfer the files to the bastion server and run the main script _setup.preflight.sh_ as defined in the [scripts/cloud-config.template.yaml](./cloud-config.template.yaml) file

If you plan securing the Bastion Server connection after the deployment, update the *HOME-CIDR* variable in the [variables.tf](./variables.tf) file before running terraform so the deployment creates a Security Rule allowing SSH connection from your IP address instead of 0.0.0.0/0 by default.

  > Note : the Bastion server is not removed at the end of the deployment as it could be used for custom configuration changes or debugging. However it is recommended to remove it once you don't need it anymore

### Cleanup

If you want to terminate the application and remove all OCI resources, run the following Terraform command:

```shell
terraform destroy     # Enter yes to validate when prompted
```

#### Topology

The following diagram shows the topology created by this project.

  > Note : The IPSec VPN is not part of the project

![Prestashop Infra](./images/prestashop-architecture.png)


#### Cost Estimator

By using the [OCI Cost Estimator](oci-cost), you can estimate the cost of the deployed resources.

For information only, the following screenshots shows a monthly estimate for such configuration.

![Prestashop Cost Estimate](./images/prestashop-cost-estimate.png)


[oci]: https://cloud.oracle.com/en_US/cloud-infrastructure
[oci-cost]: https://www.oracle.com/in/cloud/cost-estimator.html
[tf]: https://www.terraform.io
[git-project]: https://gitlab.com/solarpace/oci-prestashop
[mshell]: https://dev.mysql.com/doc/mysql-shell/8.0/en/mysql-shell-getting-started.html
