# Tags
locals {
  common_tags = {
    Reference = "Created by Guenael Voisin for Prestashop POC"
  }
}

# Available Services
data "oci_core_services" "all_services" {
  filter {
    name   = "name"
    values = ["All .* Services In Oracle Services Network"]
    regex  = true
  }
}

data oci_identity_availability_domain region_ad3 {
  compartment_id = var.compartment_ocid
  ad_number      = "3"
}

data oci_objectstorage_namespace terraform_namespace {
  compartment_id = var.compartment_ocid
}

# Cloud Init
data "template_cloudinit_config" "nodes" {
  gzip          = true
  base64_encode = true

  part {
    filename     = "cloud-config.yaml"
    content_type = "text/cloud-config"
    content      = local.cloud_init
  }
}

data "oci_waas_waas_policy" "prestashop_waas_policy" {
    #Required
    count = var.use_waf ? 1 : 0
    waas_policy_id = oci_waas_waas_policy.prestashop_waf_policy[0].id
}

locals {
  setup_preflight = file("${path.module}/scripts/setup.preflight.sh")
  setup_template = templatefile("${path.module}/scripts/setup.template.sh",
    {
#      oracle_client_version = var.oracle_client_version
  })
  deploy_template = templatefile("${path.module}/scripts/deploy.template.sh",
    {
#      oracle_client_version   = var.oracle_client_version
      mysql_user               = var.mysql_username
      mysql_password           = var.mysql_password
      mysql_ip                 = lookup(var.network_ips, "MYSQL-IP")
      psdb_dbname              = var.psdb_dbname
      psdb_username            = var.psdb_username
      psdb_password            = var.psdb_password
      prestashop_fs_data_url   = var.prestashop_fs_data_url
      psdb_par_url  = var.psdb_par_url
#      atp_pw                  = random_string.autonomous_database_admin_password.result
#      mushop_media_visibility = var.object_storage_mushop_media_visibility
#      mushop_app_par          = "https://objectstorage.${var.region}.oraclecloud.com${oci_objectstorage_preauthrequest.mushop_lite_preauth.access_uri}"
#      wallet_par              = "https://objectstorage.${var.region}.oraclecloud.com${oci_objectstorage_preauthrequest.mushop_wallet_preauth.access_uri}"
  })
  prestashopdb_sql_template = templatefile("${path.module}/scripts/prestashopdb.template.sql",
    {
      psdb_dbname   = var.psdb_dbname
      psdb_username = var.psdb_username
      psdb_password = var.psdb_password
  })
  prestashopdomain_sql_template = templatefile("${path.module}/scripts/prestashopdomain.template.sql",
    {
      ps_domain  = var.prestashop_domain
  })
  cloud_init = templatefile("${path.module}/scripts/cloud-config.template.yaml",
    {
      setup_preflight_sh_content     = base64gzip(local.setup_preflight)
      setup_template_sh_content      = base64gzip(local.setup_template)
      deploy_template_content        = base64gzip(local.deploy_template)
      prestashopdb_sql_template_content = base64gzip(local.prestashopdb_sql_template)
#      prestashopdata_sql_template_content = base64gzip(local.prestashopdata_sql_template)
      prestashopdomain_sql_template_content = base64gzip(local.prestashopdomain_sql_template)
#      httpd_conf_content             = base64gzip(local.httpd_conf)
#      mushop_media_pars_list_content = base64gzip(local.mushop_media_pars_list)
      catalogue_password             = var.mysql_password
#      catalogue_port                 = local.catalogue_port
#      catalogue_architecture         = split("/", local.compute_platform)[1]
#      mock_mode                      = var.services_in_mock_mode
#      db_name                        = oci_database_autonomous_database.mushop_autonomous_database.db_name
#      assets_url                     = var.object_storage_mushop_media_visibility == "Private" ? "" : "https://objectstorage.${var.region}.oraclecloud.com/n/${oci_objectstorage_bucket.mushop_media.namespace}/b/${oci_objectstorage_bucket.mushop_media.name}/o/"
  })
}
