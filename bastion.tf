resource oci_core_instance prestashop_bastion {
  agent_config {
    are_all_plugins_disabled = "false"
    is_management_disabled   = "false"
    is_monitoring_disabled   = "false"
    plugins_config {
      desired_state = "ENABLED"
      name          = "Compute Instance Monitoring"
    }
  }
  availability_config {
    #is_live_migration_preferred = <<Optional value not found in discovery>>
    recovery_action = "RESTORE_INSTANCE"
  }
  availability_domain = data.oci_identity_availability_domain.region_ad3.name
  compartment_id = var.compartment_ocid
  create_vnic_details {
    #assign_private_dns_record = <<Optional value not found in discovery>>
    assign_public_ip = "true"
    display_name   = "bastionvnic_prestashop_${var.environment}"
    freeform_tags = {
    }
    hostname_label = "bastionps"
    private_ip             = lookup(var.network_ips, "BASTION-IP")
    skip_source_dest_check = "false"
    subnet_id              = oci_core_subnet.prestashop_subnet_bastion.id
    #vlan_id = <<Optional value not found in discovery>>
  }
  freeform_tags  = local.common_tags
  display_name   = "bastion_prestashop_${var.environment}"
  fault_domain = var.mysql_fault_domain
  instance_options {
    are_legacy_imds_endpoints_disabled = "false"
  }
  launch_options {
    boot_volume_type                    = "PARAVIRTUALIZED"
    firmware                            = "UEFI_64"
    is_consistent_volume_naming_enabled = "true"
    is_pv_encryption_in_transit_enabled = "true"
    network_type                        = "PARAVIRTUALIZED"
    remote_data_volume_type             = "PARAVIRTUALIZED"
  }
  metadata = {
    "ssh_authorized_keys" = var.instance_public_sshkey
    user_data             = data.template_cloudinit_config.nodes.rendered
  }
  #preserve_boot_volume = <<Optional value not found in discovery>>
  shape = "VM.Standard.A1.Flex"
  shape_config {
    baseline_ocpu_utilization = ""
    memory_in_gbs             = "4"
    ocpus                     = "1"
  }
  source_details {
    source_id   = var.bastion_source_image_id
    source_type = "image"
  }
  # We should wait for the MySQL DB to be available as the Bastion imports the config
  depends_on = [oci_mysql_mysql_db_system.prestashop_mysql_db]
}
