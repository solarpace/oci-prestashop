#!/bin/bash -x
# Copyright (c) 2019-2021 Oracle and/or its affiliates. All rights reserved.
# Licensed under the Universal Permissive License v 1.0 as shown at http://oss.oracle.com/licenses/upl.
#
#
# Description: Sets up Mushop Basic a.k.a. "Monolite".
# Return codes: 0 =
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
#


# Create database and user
MYSQL_PWD=${mysql_password} mysql -u ${mysql_user}  --host ${mysql_ip} < /root/prestashopdb.sql

# Import schema and data
#echo "util.loadDump(\"${psdb_par_url}\",{\"progressFile\": \"/root/import_progress.log\"})" | mysqlsh ${mysql_user}:${mysql_password}@${mysql_ip} --schema ${psdb_dbname}

# Update Prestashop domain
#MYSQL_PWD=${mysql_password} mysql -u ${mysql_user} ${psdb_dbname} --host ${mysql_ip} < /root/prestashopdomain.sql
