create database ${psdb_dbname};

create user ${psdb_username}@localhost identified by "${psdb_password}";
create user ${psdb_username}@'%' identified by "${psdb_password}";

grant all privileges on ${psdb_dbname}.* TO ${psdb_username}@localhost;
grant all privileges on ${psdb_dbname}.* TO ${psdb_username}@'%';
