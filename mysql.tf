resource oci_mysql_mysql_db_system prestashop_mysql_db {
  admin_password      = var.mysql_password
  admin_username      = var.mysql_username
  availability_domain = data.oci_identity_availability_domain.region_ad3.name
  backup_policy {
    is_enabled        = "true"
    retention_in_days = "30"
    window_start_time = "00:00"
  }
  compartment_id          = var.compartment_ocid
#  configuration_id        = var.mysql_configuration_ocid
  data_storage_size_in_gb = "50"
  description  = "Mysql Cluster for Prestashop"
  freeform_tags  = local.common_tags
  display_name   = "mysql_prestashop_${var.environment}"
  fault_domain = var.mysql_fault_domain
  ip_address          = lookup(var.network_ips, "MYSQL-IP")
  is_highly_available = var.mysql_cluster
  maintenance {
    window_start_time = "WEDNESDAY 01:33"
  }
  port       = "3306"
  port_x     = "33060"
  shape_name = var.mysql_shape_name
  state     = "ACTIVE"
  subnet_id = oci_core_subnet.prestashop_subnet_db.id
  # https://objectstorage.eu-frankfurt-1.oraclecloud.com/p/p8tONH-NrTxmC1NGS_Jk7aKVn-7g2D1kPIPLtOuMCF5fsGwtzH07zNM-cGkbdkXz/n/oraseemeafrtech1/b/mysql-bucket/o/@.manifest.json
#  source {
#    source_type = "IMPORTURL"
#  }

  lifecycle {
    ignore_changes = [admin_password, admin_username]
  }
}
