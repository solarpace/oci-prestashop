resource oci_waas_certificate prestastop_waf_certificate {
  count = var.lb_https ? 1 : 0
    #Required
    certificate_data = var.lb_public_certificate
    compartment_id = var.compartment_ocid
    private_key_data = var.lb_private_key

    freeform_tags  = local.common_tags
    display_name   = "wafcert_prestashop_${var.environment}"
    is_trust_verification_disabled = false
#    count = var.use_waf ? 1 : 0
}

resource oci_waas_waas_policy prestashop_waf_policy {
  compartment_id = var.compartment_ocid
  freeform_tags  = local.common_tags
  display_name   = "wafpol_prestashop_${var.environment}"
  domain       = var.prestashop_domain
  count = var.use_waf ? 1 : 0

  origins {
    http_port  = "80"
    https_port = "443"
    label      = "waforigin_prestashop_${var.environment}"
    uri        = oci_load_balancer_load_balancer.prestashop_lb.ip_address_details[0].ip_address
  }
  policy_config {
    certificate_id        = var.lb_https ? oci_waas_certificate.prestastop_waf_certificate[0].id : ""
    cipher_group          = "DEFAULT"
    client_address_header = ""
    health_checks {
      expected_response_code_group = [
        "2XX",
        "3XX",
      ]
      #expected_response_text = <<Optional value not found in discovery>>
      headers = {
        "Host"       = var.prestashop_domain
        "User-Agent" = "waf health checks"
      }
      healthy_threshold              = "2"
      interval_in_seconds            = "60"
      is_enabled                     = "false"
      is_response_text_check_enabled = "false"
      method                         = "HEAD"
      path                           = "/"
      timeout_in_seconds             = "5"
      unhealthy_threshold            = "2"
    }
    is_behind_cdn                 = "false"
    is_cache_control_respected    = "false"
    is_https_enabled              = "true"
    is_https_forced               = "true"
    is_origin_compression_enabled = "true"
    is_response_buffering_enabled = "false"
    is_sni_enabled                = "false"
    load_balancing_method {
      method = "IP_HASH"
    }
    tls_protocols = [
      "TLS_V1_2",
      "TLS_V1_3",
    ]
    websocket_path_prefixes = [
    ]
  }
  waf_config {
    address_rate_limiting {
      allowed_rate_per_address      = "1"
      block_response_code           = "503"
      is_enabled                    = "false"
      max_delayed_count_per_address = "10"
    }
    device_fingerprint_challenge {
      action                       = "DETECT"
      action_expiration_in_seconds = "60"
      challenge_settings {
        block_action                 = "SHOW_ERROR_PAGE"
        block_error_page_code        = "DFC"
        block_error_page_description = "Access blocked by website owner. Please contact support."
        block_error_page_message     = "Access to the website is blocked."
        block_response_code          = "403"
        captcha_footer               = "Enter the letters and numbers as they are shown in image above."
        captcha_header               = "We have detected an increased number of attempts to access this website. To help us keep this site secure, please let us know that you are not a robot by entering the text from the image below."
        captcha_submit_label         = "Yes, I am human."
        captcha_title                = "Are you human?"
      }
      failure_threshold                       = "10"
      failure_threshold_expiration_in_seconds = "60"
      is_enabled                              = "false"
      max_address_count                       = "20"
      max_address_count_expiration_in_seconds = "60"
    }
    human_interaction_challenge {
      action                       = "DETECT"
      action_expiration_in_seconds = "60"
      challenge_settings {
        block_action                 = "SHOW_ERROR_PAGE"
        block_error_page_code        = "HIC"
        block_error_page_description = "Access blocked by website owner. Please contact support."
        block_error_page_message     = "Access to the website is blocked."
        block_response_code          = "403"
        captcha_footer               = "Enter the letters and numbers as they are shown in image above."
        captcha_header               = "We have detected an increased number of attempts to access this website. To help us keep this site secure, please let us know that you are not a robot by entering the text from the image below."
        captcha_submit_label         = "Yes, I am human."
        captcha_title                = "Are you human?"
      }
      failure_threshold                       = "10"
      failure_threshold_expiration_in_seconds = "60"
      interaction_threshold                   = "3"
      is_enabled                              = "false"
      is_nat_enabled                          = "true"
      recording_period_in_seconds             = "15"
      #set_http_header = <<Optional value not found in discovery>>
    }
    js_challenge {
      action                       = "DETECT"
      action_expiration_in_seconds = "60"
      are_redirects_challenged     = "true"
      challenge_settings {
        block_action                 = "SHOW_ERROR_PAGE"
        block_error_page_code        = "JSC-403"
        block_error_page_description = "Access blocked by website owner. Please contact support."
        block_error_page_message     = "Access to the website is blocked."
        block_response_code          = "403"
        captcha_footer               = "Enter the letters and numbers as they are shown in image above."
        captcha_header               = "We have detected an increased number of attempts to access this website. To help us keep this site secure, please let us know that you are not a robot by entering the text from the image below."
        captcha_submit_label         = "Yes, I am human."
        captcha_title                = "Are you human?"
      }
      failure_threshold = "10"
      is_enabled        = "false"
      is_nat_enabled    = "true"
      set_http_header {
        name  = "x-jsc-alerts"
        value = "{failed_amount}"
      }
    }
    origin = "waforigin_prestashop_${var.environment}"
    origin_groups = [
    ]
    protection_settings {
      allowed_http_methods = [
        "GET",
        "POST",
        "HEAD",
        "OPTIONS",
      ]
      block_action                       = "SET_RESPONSE_CODE"
      block_error_page_code              = "403"
      block_error_page_description       = "Access blocked by website owner. Please contact support."
      block_error_page_message           = "Access to the website is blocked."
      block_response_code                = "403"
      is_response_inspected              = "false"
      max_argument_count                 = "255"
      max_name_length_per_argument       = "400"
      max_response_size_in_ki_b          = "1024"
      max_total_name_length_of_arguments = "64000"
      media_types = [
        "text/html",
        "text/plain",
      ]
      recommendations_period_in_days = "10"
    }
  }
}
