resource oci_file_storage_export_set prestashop_export_set {
    #Required
    mount_target_id = oci_file_storage_mount_target.prestashop_mounttarget.id

    display_name   = "exportset_prestashop_${var.environment}"
    max_fs_stat_bytes = 23843202333
    max_fs_stat_files = 223442
}

resource oci_file_storage_export prestashop_export {
  export_options {
    access                         = "READ_WRITE"
    anonymous_gid                  = "65534"
    anonymous_uid                  = "65534"
    identity_squash                = "NONE"
    require_privileged_source_port = "false"
    source                         = "0.0.0.0/0"
  }
  export_set_id  = oci_file_storage_export_set.prestashop_export_set.id
  file_system_id = oci_file_storage_file_system.prestashop_fs.id
  path           = "/fs-prestashop"
}

resource oci_file_storage_file_system prestashop_fs {
  availability_domain = data.oci_identity_availability_domain.region_ad3.name
  compartment_id      = var.compartment_ocid
  freeform_tags  = local.common_tags
  display_name   = "fs_prestashop_${var.environment}"
  kms_key_id = ""
}

resource oci_file_storage_mount_target prestashop_mounttarget {
  availability_domain = data.oci_identity_availability_domain.region_ad3.name
  compartment_id      = var.compartment_ocid
  freeform_tags  = local.common_tags
  display_name   = "mounttgt_prestashop_${var.environment}"
  ip_address = lookup(var.network_ips, "FS-IP")
  subnet_id = oci_core_subnet.prestashop_subnet_app.id
}

resource oci_objectstorage_bucket prestashop_custom_images {
  access_type    = "NoPublicAccess"
  auto_tiering   = "Disabled"
  compartment_id = var.compartment_ocid
  freeform_tags  = local.common_tags
  name                  = "bucket-custimg-prestashop_${var.environment}"
  namespace             = data.oci_objectstorage_namespace.terraform_namespace.namespace
  object_events_enabled = "false"
  storage_tier          = "Standard"
  versioning            = "Disabled"
}
