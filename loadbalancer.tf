resource oci_load_balancer_load_balancer prestashop_lb {
  compartment_id = var.compartment_ocid
  freeform_tags  = local.common_tags
  display_name   = "lb_prestashop_${var.environment}"
  ip_mode    = "IPV4"
  is_private = "false"
  shape = "flexible"
  shape_details {
    maximum_bandwidth_in_mbps = "10"
    minimum_bandwidth_in_mbps = "10"
  }
  subnet_ids = [
    oci_core_subnet.prestashop_subnet_lb.id,
  ]
}

resource oci_load_balancer_certificate prestashop_certificate {
  count = var.lb_https ? 1 : 0
  certificate_name   = "cert_prestashop_${var.environment}"
  load_balancer_id = oci_load_balancer_load_balancer.prestashop_lb.id
  #passphrase = <<Optional value not found in discovery>>
  private_key = var.lb_private_key
  public_certificate = var.lb_public_certificate
}

resource oci_load_balancer_backend_set prestashop_backendset {
  health_checker {
    interval_ms         = "10000"
    port                = "80"
    protocol            = "HTTP"
    response_body_regex = ""
    retries             = "2"
    return_code         = "200"
    timeout_in_millis   = "3000"
    url_path            = "/gv.html"
  }
  load_balancer_id = oci_load_balancer_load_balancer.prestashop_lb.id
  name   = "bset_prestashop_${var.environment}"
  policy           = "ROUND_ROBIN"
  session_persistence_configuration {
    cookie_name      = "*"
    disable_fallback = "false"
  }
}

resource oci_load_balancer_listener prestashop_listener_http {
  connection_configuration {
    backend_tcp_proxy_protocol_version = "0"
    idle_timeout_in_seconds            = "60"
  }
  default_backend_set_name = oci_load_balancer_backend_set.prestashop_backendset.name
  load_balancer_id = oci_load_balancer_load_balancer.prestashop_lb.id
  name   = "lst_prestashop-http_${var.environment}"
  port     = "80"
  protocol = "HTTP"
  rule_set_names = [
  ]
}

resource oci_load_balancer_listener prestashop_listener_https {
  count = var.lb_https ? 1 : 0
  connection_configuration {
    backend_tcp_proxy_protocol_version = "0"
    idle_timeout_in_seconds            = "60"
  }
  default_backend_set_name = oci_load_balancer_backend_set.prestashop_backendset.name
  hostname_names = [
  ]
  load_balancer_id = oci_load_balancer_load_balancer.prestashop_lb.id
  name   = "lst_prestashop-https_${var.environment}"
  port     = "443"
  protocol = "HTTP"
  ssl_configuration {
    certificate_name  = oci_load_balancer_certificate.prestashop_certificate[0].certificate_name
    cipher_suite_name = "oci-default-ssl-cipher-suite-v1"
    protocols = [
      "TLSv1.2",
    ]
    server_order_preference = "DISABLED"
    verify_depth            = "1"
    verify_peer_certificate = "false"
  }
}
