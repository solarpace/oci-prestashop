resource oci_core_instance_configuration prestashop_instconfig {
  count = var.create_appserver ? 1 : 0
  compartment_id = var.compartment_ocid
  freeform_tags  = local.common_tags
  display_name   = "instconfig_prestashop_${var.environment}"
  instance_details {
    instance_type = "compute"
    launch_details {
      agent_config {
        are_all_plugins_disabled = "false"
        is_management_disabled   = "false"
        is_monitoring_disabled   = "false"
        plugins_config {
          desired_state = "ENABLED"
          name          = "Vulnerability Scanning"
        }
        plugins_config {
          desired_state = "ENABLED"
          name          = "Custom Logs Monitoring"
        }
        plugins_config {
          desired_state = "ENABLED"
          name          = "Compute Instance Monitoring"
        }
        plugins_config {
          desired_state = "DISABLED"
          name          = "Bastion"
        }
      }
      availability_config {
        recovery_action = "RESTORE_INSTANCE"
      }
      compartment_id = var.compartment_ocid
      create_vnic_details {
        assign_private_dns_record = "true"
        assign_public_ip          = "false"
        freeform_tags  = local.common_tags
        display_name   = "oci-ps-d-web_${var.environment}"
        skip_source_dest_check = "false"
      }
      freeform_tags  = local.common_tags
      display_name   = "oci-ps-d-web"
      instance_options {
        are_legacy_imds_endpoints_disabled = "false"
      }
      is_pv_encryption_in_transit_enabled = "false"
      launch_mode                         = "NATIVE"
      launch_options {
        boot_volume_type = ""
        firmware         = ""
        network_type            = "VFIO"
        remote_data_volume_type = ""
      }
      metadata = {
        "ssh_authorized_keys" = var.instance_public_sshkey
      }
      preferred_maintenance_action = ""
      shape                        = var.instance_shape
      shape_config {
        baseline_ocpu_utilization = ""
        memory_in_gbs             = "8"
        ocpus                     = "1"
      }
      source_details {
        image_id    = oci_core_image.prestashop_custimg[0].id
        source_type = "image"
      }
    }
  }
}

resource oci_core_image prestashop_custimg {
  count = var.create_appserver ? 1 : 0
  compartment_id = var.compartment_ocid
  freeform_tags  = local.common_tags
  display_name   = "custimg_prestashop_${var.environment}"
  launch_mode = "NATIVE"
  image_source_details {
#      source_type = "objectStorageTuple"
#      bucket_name = var.bucket_name
#      namespace_name = var.namespace
#      object_name = var.object_name # exported image name

      source_type = "objectStorageUri"
      source_uri = var.prestashop_custimg_url
      #Optional
      operating_system = var.image_image_source_details_operating_system
      operating_system_version = var.image_image_source_details_operating_system_version
      source_image_type = var.source_image_type
  }
}

resource oci_core_instance_pool prestashop_instpool {
  count = var.create_appserver ? 1 : 0
  compartment_id = var.compartment_ocid
  freeform_tags  = local.common_tags
  display_name   = "instpool_prestashop_${var.environment}"
  instance_configuration_id = oci_core_instance_configuration.prestashop_instconfig[0].id
  load_balancers {
    backend_set_name = oci_load_balancer_backend_set.prestashop_backendset.name
    load_balancer_id = oci_load_balancer_load_balancer.prestashop_lb.id
    port             = "80"
    vnic_selection   = "PrimaryVnic"
  }
  placement_configurations {
    availability_domain = data.oci_identity_availability_domain.region_ad3.name
    fault_domains = [
    ]
    primary_subnet_id = oci_core_subnet.prestashop_subnet_app.id
  }
  size  = "2"
  state = "RUNNING"
}


resource oci_autoscaling_auto_scaling_configuration prestashop_autoscaling {
  count = var.create_appserver ? 1 : 0
  auto_scaling_resources {
    id   = oci_core_instance_pool.prestashop_instpool[0].id
    type = "instancePool"
  }
  compartment_id       = var.compartment_ocid
  cool_down_in_seconds = "300"
  freeform_tags  = local.common_tags
  display_name   = "autoscaling_prestashop_${var.environment}"
  is_enabled = "true"
  policies {
    capacity {
      initial = "2"
      max     = "4"
      min     = "2"
    }
    display_name   = "autoscalingpol_prestashop_${var.environment}"
    is_enabled  = "true"
    policy_type = "threshold"
    #resource_action = <<Optional value not found in discovery>>
    rules {
      action {
        type  = "CHANGE_COUNT_BY"
        value = "-1"
      }
      display_name   = "autoscalingcond_prestashop-rem_${var.environment}"
      metric {
        metric_type = "CPU_UTILIZATION"
        threshold {
          operator = "LT"
          value    = "40"
        }
      }
    }
    rules {
      action {
        type  = "CHANGE_COUNT_BY"
        value = "1"
      }
      display_name   = "autoscalingcond_prestashop-add_${var.environment}"
      metric {
        metric_type = "CPU_UTILIZATION"
        threshold {
          operator = "GT"
          value    = "60"
        }
      }
    }
  }
}
